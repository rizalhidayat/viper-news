//
//  UITableViewExtension.swift
//  Viper News
//
//  Created by Rizal Hidayat on 18/09/22.
//

import UIKit

extension UITableView {
    func setEmptyMessage(_ message: String) {
            let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
            messageLabel.text = message
            messageLabel.textColor = .secondaryLabel
            messageLabel.numberOfLines = 0
            messageLabel.textAlignment = .center
            messageLabel.sizeToFit()
            self.backgroundView = messageLabel
        }
}
