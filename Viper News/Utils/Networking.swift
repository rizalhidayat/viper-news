//
//  Networking.swift
//  Viper News
//
//  Created by Rizal Hidayat on 18/09/22.
//

import Foundation
import Alamofire

class Networking {
    static let apiKey = "92dcfbca2e814606b45526c6e0a37e11"
    
    static func getHTTPHeader() -> HTTPHeaders {
        let headers: HTTPHeaders = ["X-Api-Key": apiKey]
        return headers
    }
}
