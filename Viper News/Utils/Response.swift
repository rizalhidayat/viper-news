//
//  Response.swift
//  Viper News
//
//  Created by Rizal Hidayat on 17/09/22.
//

import Foundation

struct Response<R: Decodable> : Decodable {
    let status: Status
    let totalResults: Int?
    let result: R?
    let code: String?
    let message: String?
    
    enum Status: String, Decodable {
        case ok = "ok"
        case error = "error"
    }
        
    struct ResultDynamicKey: CodingKey {
        var stringValue: String
        
        init?(stringValue: String) {
            self.stringValue = stringValue
        }
        
        var intValue: Int?
        
        init?(intValue: Int) {
            self.intValue = intValue
            self.stringValue = String(intValue)
        }
        
    }
    
    init(from decoder: Decoder) throws {
        var dynamicResult: R?
        var status: Status = .error
        var totalResults: Int?
        var code: String?
        var message: String?
        
        let dynamicContainer = try decoder.container(keyedBy: ResultDynamicKey.self)
        try dynamicContainer.allKeys.forEach{ key in
            switch key.stringValue {
            case "status":
                status = try dynamicContainer.decode(Status.self, forKey: key)
            case "totalResults":
                totalResults = try dynamicContainer.decode(Int.self, forKey: key)
            case "code":
                code = try dynamicContainer.decode(String.self, forKey: key)
            case "message":
                message = try dynamicContainer.decode(String.self, forKey: key)
            default:
                dynamicResult = try dynamicContainer.decode(R.self, forKey: key)
            }
        }
        self.status = status
        self.totalResults = totalResults
        self.result = dynamicResult
        self.code = code
        self.message = message
        
    }
}



