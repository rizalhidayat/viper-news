//
//  DateFormatter.swift
//  Viper News
//
//  Created by Rizal Hidayat on 18/09/22.
//

import Foundation

extension String {
    func formatDate(from currentFormat: String, to expectedFormat: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = currentFormat
        guard let date = formatter.date(from: self) else { return self }
        formatter.dateFormat = expectedFormat
        return formatter.string(from: date)
    }
}
