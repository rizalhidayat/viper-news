//
//  NewsCategoryRouter.swift
//  Viper News
//
//  Created by Rizal Hidayat on 17/09/22.
//

import UIKit

protocol NewsCategoryRouterProtocol {
    func navigateToNewsSource(with category: String)
}

class NewsCategoryRouter: NewsCategoryRouterProtocol {
    let view: NewsCategoryViewController
    
    init(view: NewsCategoryViewController) {
        self.view = view
    }
    
    func navigateToNewsSource(with category: String) {
        let viewController = NewsSourceViewController()
        let interactor = NewsSourceInteractor()
        let router = NewsSourceRouter(view: viewController)
        
        let presenter = NewsSourcePresenter(view: viewController, interactor: interactor, router: router, category: category)
        viewController.presenter = presenter
        
        view.navigationController?.pushViewController(viewController, animated: true)
    }
}
