//
//  NewsCategoryViewController.swift
//  Viper News
//
//  Created by Rizal Hidayat on 17/09/22.
//

import UIKit

protocol NewsCategoryViewProtocol {
    func showCategoryList()
}

class NewsCategoryViewController: UIViewController {

    @IBOutlet weak var categoryTableView: UITableView!
    
    var presenter: NewsCategoryPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initNavigationBar()
        initTableView()
        presenter.loadCategories()
    }
    
    private func initNavigationBar(){
        navigationController?.navigationBar.prefersLargeTitles = true
        title = "News Category"
    }
    
    private func initTableView(){
        categoryTableView.delegate = self
        categoryTableView.dataSource = self
    }

}

extension NewsCategoryViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.categories?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = presenter.categories?[indexPath.row].capitalized
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        presenter.selectCategory(of: indexPath.row)
    }
}

extension NewsCategoryViewController:  NewsCategoryViewProtocol {
    func showCategoryList() {
        categoryTableView.reloadData()
    }
}
