//
//  NewsCategoryPresenter.swift
//  Viper News
//
//  Created by Rizal Hidayat on 17/09/22.
//

import Foundation

protocol NewsCategoryPresenterProtocol {
    var categories: [String]? { get }
    func loadCategories()
    func selectCategory(of index: Int)
}

class NewsCategoryPresenter: NewsCategoryPresenterProtocol {
    var categories: [String]?
    
    let view: NewsCategoryViewProtocol
    let interactor: NewsCategoryInteractorProtocol
    let router: NewsCategoryRouterProtocol
    
    init(view: NewsCategoryViewProtocol, interactor: NewsCategoryInteractorProtocol, router: NewsCategoryRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func loadCategories(){
        categories = interactor.getAllCategories()
        view.showCategoryList()
    }
    
    func selectCategory(of index: Int) {
        guard let categories = categories else { return }
        router.navigateToNewsSource(with: categories[index])
    }
}
