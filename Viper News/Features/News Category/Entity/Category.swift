//
//  Category.swift
//  Viper News
//
//  Created by Rizal Hidayat on 17/09/22.
//

import Foundation

enum Category: String, Decodable, CaseIterable {
    case business = "business"
    case entertainment = "entertainment"
    case general = "general"
    case health = "health"
    case science = "science"
    case sports = "sports"
    case technology = "technology"
}
