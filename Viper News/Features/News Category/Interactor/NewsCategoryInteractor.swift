//
//  NewsCategoryInteractor.swift
//  Viper News
//
//  Created by Rizal Hidayat on 17/09/22.
//

import Foundation

protocol NewsCategoryInteractorProtocol {
    func getAllCategories() -> [String]
}

class NewsCategoryInteractor: NewsCategoryInteractorProtocol {
    func getAllCategories() -> [String] {
        return Category.allCases.compactMap { $0.rawValue }
    }
}
