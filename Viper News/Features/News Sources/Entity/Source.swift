//
//  Source.swift
//  Viper News
//
//  Created by Rizal Hidayat on 17/09/22.
//

import Foundation

// MARK: - Source
struct Source: Decodable {
    let id, name, description: String?
    let url: String?
    let category: Category?
    let language, country: String?
}
