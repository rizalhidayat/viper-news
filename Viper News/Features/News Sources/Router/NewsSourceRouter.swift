//
//  NewsSourceRouter.swift
//  Viper News
//
//  Created by Rizal Hidayat on 18/09/22.
//

import UIKit

protocol NewsSourceRouterProtocol {
    func navigateToNewsArticles(with source: String)
}

class NewsSourceRouter: NewsSourceRouterProtocol {
    let view: NewsSourceViewController
    
    init(view: NewsSourceViewController){
        self.view = view
    }
    
    func navigateToNewsArticles(with source: String) {
        let viewController = NewsArticleViewController()
        let interactor = NewsArticleInteractor()
        let router = NewsArticleRouter(view: viewController)
        
        let presenter = NewsArticlePresenter(view: viewController, interactor: interactor, router: router, source: source)
        viewController.presenter = presenter
        view.navigationController?.pushViewController(viewController, animated: true)
    }
    
}
