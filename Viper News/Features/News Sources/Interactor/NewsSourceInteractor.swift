//
//  NewsSourceInteractor.swift
//  Viper News
//
//  Created by Rizal Hidayat on 18/09/22.
//

import Foundation
import Alamofire

protocol NewsSourceInteractorProtocol {
    func getNewsSources(with category: String, completion: @escaping ([Source]?, String?) -> Void)
}

class NewsSourceInteractor: NewsSourceInteractorProtocol {
    func getNewsSources(with category: String, completion: @escaping ([Source]?, String?) -> Void) {
        let url = "https://newsapi.org/v2/top-headlines/sources?category=\(category)"
        AF.request(url, method: .get, headers: Networking.getHTTPHeader()).responseDecodable(of: Response<[Source]>.self) { response in
            switch response.result {
            case let .success(data):
                switch data.status {
                case .ok:
                    completion(data.result, nil)
                case .error:
                    completion(nil, data.message)
                }
            case let .failure(error):
                completion(nil, error.localizedDescription)
            }
        }
    }
    

}
