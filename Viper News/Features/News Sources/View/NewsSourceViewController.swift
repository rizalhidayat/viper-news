//
//  NewsSourceViewController.swift
//  Viper News
//
//  Created by Rizal Hidayat on 18/09/22.
//

import UIKit

protocol NewsSourceViewProtocol {
    func showSourceList()
    func showErrorMessage(message: String)
    func showLoading(show: Bool)
}

class NewsSourceViewController: UIViewController {
    
    @IBOutlet weak var sourcesTableView: UITableView!
    @IBOutlet weak var loadingIndicatorView: UIActivityIndicatorView!
    
    var presenter: NewsSourcePresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.fetchNewsSources()
        initTableView()
        initNavigationBar()
    }
    
    private func initNavigationBar() {
        navigationItem.largeTitleDisplayMode = .never
        title = presenter.category.capitalized
    }
    
    private func initTableView(){
        sourcesTableView.setEmptyMessage("There are no news sources that can be displayed.")
        sourcesTableView.dataSource = self
        sourcesTableView.delegate = self
        sourcesTableView.register(UINib(nibName: "NewsSourceTableViewCell", bundle: nil), forCellReuseIdentifier: "NewsSourceTableViewCell")
    }
    
}

extension NewsSourceViewController: NewsSourceViewProtocol {
    func showLoading(show: Bool) {
        if show {
            loadingIndicatorView.startAnimating()
        } else {
            loadingIndicatorView.stopAnimating()
        }
    }
    
    func showErrorMessage(message: String) {
        let alert = UIAlertController(title: "Oops.", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default))
        self.present(alert, animated: true)
    }
    
    func showSourceList() {
        sourcesTableView.reloadData()
    }
    
    
}

extension NewsSourceViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.newsSources?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsSourceTableViewCell", for: indexPath) as? NewsSourceTableViewCell,
              let source = presenter.newsSources?[indexPath.row] else {
            return UITableViewCell()
        }
        cell.configure(with: source)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        presenter.selectNewsSource(of: indexPath.row)
    }
}
