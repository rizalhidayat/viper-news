//
//  NewsSourceTableViewCell.swift
//  Viper News
//
//  Created by Rizal Hidayat on 18/09/22.
//

import UIKit

class NewsSourceTableViewCell: UITableViewCell {
    @IBOutlet weak var sourceNameLabel: UILabel!
    @IBOutlet weak var sourceDescriptionLabel: UILabel!
    @IBOutlet weak var sourceCountryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(with source: Source){
        sourceNameLabel.text = source.name ?? "-"
        sourceDescriptionLabel.text = source.description ?? "-"
        sourceCountryLabel.text = source.country ?? "-"
    }
    
}
