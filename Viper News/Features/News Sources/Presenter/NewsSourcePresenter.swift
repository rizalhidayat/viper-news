//
//  NewsSourcePresenter.swift
//  Viper News
//
//  Created by Rizal Hidayat on 18/09/22.
//

import Foundation

protocol NewsSourcePresenterProtocol {
    func fetchNewsSources()
    func selectNewsSource(of index: Int)
    var category: String { get }
    var newsSources: [Source]? { get }
}

class NewsSourcePresenter: NewsSourcePresenterProtocol {
    let view: NewsSourceViewProtocol
    let interactor: NewsSourceInteractorProtocol
    let router: NewsSourceRouterProtocol
    
    let category: String
    var newsSources: [Source]?
    
    init(view: NewsSourceViewProtocol, interactor: NewsSourceInteractorProtocol, router: NewsSourceRouterProtocol, category: String){
        self.view = view
        self.interactor = interactor
        self.router = router
        self.category = category
    }
    
    func fetchNewsSources() {
        view.showLoading(show: true)
        interactor.getNewsSources(with: category) { sources, error  in
            self.view.showLoading(show: false)
            if let error = error {
                self.view.showErrorMessage(message: error)
                return
            }
            self.newsSources = sources
            self.view.showSourceList()
        }
    }
    
    func selectNewsSource(of index: Int) {
        guard let source = newsSources?[index], let sourceId = source.id else { return }
        router.navigateToNewsArticles(with: sourceId)
    }
}
