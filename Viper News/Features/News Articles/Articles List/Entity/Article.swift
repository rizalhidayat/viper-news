//
//  Article.swift
//  Viper News
//
//  Created by Rizal Hidayat on 17/09/22.
//

import Foundation

struct Article: Decodable {
    let source: Source?
    let author, title, description: String?
    let url: String?
    let urlToImage: String?
    let publishedAt, content: String?
    
    // MARK: - Source
    struct Source: Decodable {
        let id, name: String?
    }

}

