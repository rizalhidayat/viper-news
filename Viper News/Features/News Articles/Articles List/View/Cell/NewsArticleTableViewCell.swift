//
//  NewsArticleTableViewCell.swift
//  Viper News
//
//  Created by Rizal Hidayat on 18/09/22.
//

import UIKit
import Kingfisher

class NewsArticleTableViewCell: UITableViewCell {
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var articleDateLabel: UILabel!
    @IBOutlet weak var articleTitleLabel: UILabel!
    @IBOutlet weak var articleDescriptionLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var readMoreButton: UIButton!
    
    var callback: ((String) -> Void)?
    var newsUrl: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initListener()
    }
    
    private func initListener() {
        readMoreButton.addTarget(self, action: #selector(readMore), for: .touchUpInside)
    }
    
    @objc
    private func readMore(){
        if let stringUrl = newsUrl {
            callback?(stringUrl)
        }
    }
    
    func configure(with article: Article){
        articleDateLabel.text = article.publishedAt?.formatDate(from: "yyyy-MM-dd'T'HH:mm:ssZ", to: "MMM d, yyyy, hh:mm a")
        articleTitleLabel.text = article.title
        articleDescriptionLabel.text = article.description
        authorNameLabel.text = article.author
        newsUrl = article.url
        if let stringUrl = article.urlToImage, let imageUrl = URL(string: stringUrl) {
            articleImageView.isHidden = false
            articleImageView.kf.setImage(with: imageUrl)
        }
    }
    
}
