//
//  NewsArticleViewController.swift
//  Viper News
//
//  Created by Rizal Hidayat on 18/09/22.
//

import UIKit

protocol NewsArticleViewProtocol {
    func showArticleList()
    func showErrorMessage(message: String)
    func showLoading(show: Bool)
    
}

class NewsArticleViewController: UIViewController {

    @IBOutlet weak var articleTableView: UITableView!
    @IBOutlet weak var loadingIndicatorView: UIActivityIndicatorView!
    var presenter: NewsArticlePresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.fetchArticles()
        initNavigationBar()
        initTableView()
    }
    
    private func initNavigationBar(){
        navigationItem.largeTitleDisplayMode = .never
        title = "Articles"
    }
    
    private func initTableView(){
        articleTableView.dataSource = self
        articleTableView.delegate = self
        articleTableView.register(UINib(nibName: "NewsArticleTableViewCell", bundle: nil), forCellReuseIdentifier: "NewsArticleTableViewCell")
        articleTableView.setEmptyMessage("There are no articles that can be displayed.")
    }

}

extension NewsArticleViewController: NewsArticleViewProtocol {
    func showArticleList() {
        articleTableView.reloadData()
    }
    
    func showErrorMessage(message: String) {
        let alert = UIAlertController(title: "Oops.", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default))
        self.present(alert, animated: true)
    }
    
    func showLoading(show: Bool) {
        if show {
            loadingIndicatorView.startAnimating()
        } else {
            loadingIndicatorView.stopAnimating()
        }
    }
}

extension NewsArticleViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.articles?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsArticleTableViewCell", for: indexPath) as? NewsArticleTableViewCell,
              let article = presenter.articles?[indexPath.row] else { return UITableViewCell() }
        cell.configure(with: article)
        cell.callback = { url in
            self.presenter.readArticle(on: url)
        }
        presenter.loadNextPage(lastIndex: indexPath.row)
        return cell
    }
}
