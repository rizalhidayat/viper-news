//
//  NewsArticleInteractor.swift
//  Viper News
//
//  Created by Rizal Hidayat on 18/09/22.
//

import Foundation
import Alamofire

protocol NewsArticleInteractorProtocol {
    func getNewsArticles(with source: String, page: Int, pageSize: Int, completion: @escaping ([Article]?, Int?, String?) -> Void)
}

class NewsArticleInteractor: NewsArticleInteractorProtocol {
    func getNewsArticles(with source: String, page: Int, pageSize: Int, completion: @escaping ([Article]?, Int?, String?) -> Void) {
        let url = "https://newsapi.org/v2/everything?sources=\(source)&page=\(page)&pageSize=\(pageSize)"
        AF.request(url, method: .get, headers: Networking.getHTTPHeader()).responseDecodable(of: Response<[Article]>.self) { response in
            switch response.result {
            case let .success(data):
                switch data.status {
                case .ok:
                    completion(data.result, data.totalResults, nil)
                case .error:
                    completion(nil, nil, data.message)
                }
            case let .failure(error):
                completion(nil, nil, error.localizedDescription)
            }
        }
    }
    
}

