//
//  NewsArticlePresenter.swift
//  Viper News
//
//  Created by Rizal Hidayat on 18/09/22.
//

import Foundation

protocol NewsArticlePresenterProtocol {
    func fetchArticles()
    func loadNextPage(lastIndex: Int)
    func readArticle(on url: String)
    var articles: [Article]? { get }
}

class NewsArticlePresenter: NewsArticlePresenterProtocol {
    let view: NewsArticleViewProtocol
    let interactor: NewsArticleInteractorProtocol
    let router: NewsArticleRouterProtocol
    let source: String
    
    var articles: [Article]?
    
    //MARK: - Pagination Variable
    let pageSize = 5
    var currentPage = 1
    var totalData = 0
    var canLoadNextPage = false
    
    init(view: NewsArticleViewProtocol, interactor: NewsArticleInteractorProtocol, router: NewsArticleRouterProtocol, source: String){
        self.view = view
        self.interactor = interactor
        self.router = router
        self.source = source
    }
    
    func fetchArticles() {
        canLoadNextPage = false
        self.view.showLoading(show: true)
        interactor.getNewsArticles(with: source, page: currentPage, pageSize: pageSize) { articles, totalData, error in
            self.view.showLoading(show: false)
            if let error = error {
                self.view.showErrorMessage(message: error)
                return
            }
            if self.currentPage == 1 {
                self.totalData = totalData ?? 0
                self.articles = articles
            } else {
                self.articles?.append(contentsOf: articles ?? [])
            }
            self.canLoadNextPage = true
            self.currentPage += 1
            self.view.showArticleList()
        }
    }
    
    func loadNextPage(lastIndex: Int) {
        let totalPage = (totalData % pageSize == 0) ? totalData / pageSize : totalData / pageSize + 1
        if currentPage <= totalPage && canLoadNextPage {
            let totalData = (articles?.count ?? 1) - 1
            if lastIndex == totalData {
                fetchArticles()
            }
        }
    }
    
    func readArticle(on url: String) {
        router.navigateToArticleDetail(with: url)
    }
    
}
