//
//  NewsArticleRouter.swift
//  Viper News
//
//  Created by Rizal Hidayat on 18/09/22.
//

import Foundation

protocol NewsArticleRouterProtocol {
    func navigateToArticleDetail(with url: String)
}

class NewsArticleRouter: NewsArticleRouterProtocol {
    let view: NewsArticleViewController
    
    init(view: NewsArticleViewController) {
        self.view = view
    }
    
    func navigateToArticleDetail(with url: String) {
        let viewController = NewsArticleDetailViewController()
        viewController.urlAddress = url
        view.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
}
