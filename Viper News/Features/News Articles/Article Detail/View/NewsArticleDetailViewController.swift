//
//  NewsArticleDetailViewController.swift
//  Viper News
//
//  Created by Rizal Hidayat on 18/09/22.
//

import UIKit
import WebKit

class NewsArticleDetailViewController: UIViewController {
    @IBOutlet weak var webView: WKWebView!
    var urlAddress: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initNavigationBar()
        loadWebView()
    }
    
    private func initNavigationBar() {
        title = "Article"
    }

    private func loadWebView() {
        guard let urlString = urlAddress, let url = URL(string: urlString) else { return }
        webView.load(URLRequest(url: url))
    }
}
